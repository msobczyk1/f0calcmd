# f0calcmd



## Getting started

To start the server:

```python
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install pip --upgrade
python3 -m pip install -r requirements.txt
python3 -m flask --app f0calcmd run
```

On the Artemis side make sure you have in `server.yml` config:
```yaml
pools:
  - name: rest
    driver: rest
    parameters:
      url: "http://localhost:5000"
      capabilities:
        supported-architectures:
          - x86_64
```
