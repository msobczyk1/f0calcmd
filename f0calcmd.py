import json
import flask.json

from flask import Flask, request

app = Flask(__name__)
app.debug = True

GUESTS = {"g1", "g2", "g3"}
RESERVED_GUESTS = set()

@app.route("/fetch_pool_resources_metrics", methods=["GET"])
def fetch_pool_resources():
    return flask.json.jsonify({
        "usage": {
            "instances": 5,
            "cores": 20,
            "memory": 100,
        }
    })

@app.route("/guests", methods=["GET"])
def can_acquire():
    # print(f"request data: {request.json}")
    if not GUESTS:
        return flask.json.jsonify({
            "result": False,
            "reason": "no guests left"
        })
    else:
        return flask.json.jsonify({
            "result": True
        })

@app.route("/guests", methods=["POST"])
def acquire_guest():
    # print(f"request data: {request.json}")
    name = next(iter(GUESTS))
    RESERVED_GUESTS.add(name)
    GUESTS.remove(name)
    pool_data = json.dumps({"name": name})
    result = {
        "state": "pending",
        "pool_data": pool_data,
    }
    print(f"Reserved {name} guest, all reserved: {RESERVED_GUESTS}, all free: {GUESTS}")
    return flask.json.jsonify(result)

@app.route("/guests", methods=["PUT"])
def update_guest():
    # print(f"request data: {request.json}")
    raw_pool_data = request.json["pool_data"]
    pool_data = json.loads(raw_pool_data)
    name = pool_data["name"]
    print(f"Updating {name} guest")
    result = {
        "state": "complete",
        "pool_data": raw_pool_data,
        "address": "127.0.0.1",
    }
    return flask.json.jsonify(result)

@app.route("/guests", methods=["DELETE"])
def release_guest():
    # print(f"request data: {request.json}")
    raw_pool_data = request.json["pool_data"]
    pool_data = json.loads(raw_pool_data)
    name = pool_data["name"]
    if name in RESERVED_GUESTS:
        RESERVED_GUESTS.remove(name)
        GUESTS.add(name)
        print(f"Releasing {name} guest, all reserved: {RESERVED_GUESTS}, all free: {GUESTS}")
        return flask.json.jsonify({
            "result": True,
        })
    else:
        return flask.json.jsonify({
            "result": False,
        })
